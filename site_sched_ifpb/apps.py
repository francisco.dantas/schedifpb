from django.apps import AppConfig


class SiteSchedIfpbConfig(AppConfig):
    name = 'site_sched_ifpb'
